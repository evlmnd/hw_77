import React, {Component} from 'react';
import {getMessages} from "../../store/actions";
import {connect} from "react-redux";
import Message from "../../components/Message/Message";
import './List.css';

class List extends Component {

    componentDidMount() {
        this.props.onGetMessages();
    }

    componentDidUpdate() {
        if (this.props.imageLoaded) {
            this.props.onGetMessages();
            this.props.completeLoadingImage();
        }
    }

    render() {
        return (
            <div className="List">
                {this.props.messages.map(message => {
                    const date = (message.datetime.slice(0, 10) + ', ' + message.datetime.slice(11, 16));
                    return <Message
                        author={message.author}
                        message={message.message}
                        datetime={date}
                        image={message.image}
                        key={message.id}
                    />
                })}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        messages: state.messages,
        imageLoaded: state.imageLoaded
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetMessages: () => dispatch(getMessages()),
        completeLoadingImage: () => dispatch({type: 'COMPLETE_LOADING_IMAGE'})
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(List);
