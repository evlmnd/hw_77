import React from 'react';
import {apiURL} from "../../constants";


const styles = {
    width: '100px',
    height: 'auto',
    marginRight: '10px'
};

const ImageThumbnail = (props) => {
    if (props.image) {
        const image = apiURL + '/uploads/' + props.image;
        return (<img src={image} alt="img" style={styles}/>);
    } else {
        return null;
    }
};

export default ImageThumbnail;