import React, {Component} from 'react';
import {getMessages, sendMessage} from "../../store/actions";
import {connect} from "react-redux";
import './Form.css';


class Form extends Component {

    state = {
        author: '',
        message: '',
        image: ''
    };

    inputChangeHandler = event => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    onSubmitForm = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.props.onSubmit(formData);

        this.setState({author: '', message: '', image: ''});
        this.props.getMessages();
    };

    render() {
        return (
            <div className="Form">
                <div className="container">
                    <form onSubmit={this.onSubmitForm}>
                        <input type="text" placeholder="Author" name="author" onChange={this.inputChangeHandler} value={this.state.author}/>
                        <input type="text" placeholder="Message" name="message" onChange={this.inputChangeHandler} value={this.state.message}/>
                        <input
                            placeholder="Add file"
                            type="file"
                            name="image" id="image"
                            onChange={this.fileChangeHandler}
                        />
                        <button type="submit">Save</button>
                    </form>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {

    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: formData => dispatch(sendMessage(formData)),
        getMessages: () => dispatch(getMessages())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);