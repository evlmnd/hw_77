import React from 'react';
import ImageThumbnail from "../ImageThumbnail/ImageThumbnail";
import './Message.css';



const Message = (props) => {
    return (
        <div className="Message">
            <p>[ <b>{props.author}</b> ]:  <span>{props.datetime}</span></p>
            <p>{props.message}</p>
            <ImageThumbnail image={props.image}/>
        </div>
)
    ;
};

export default Message;