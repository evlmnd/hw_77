import React, {Component, Fragment} from 'react';
import List from "./containers/List/List";
import Form from "./components/Form/Form";

class App extends Component {
    render() {
        return (
            <Fragment>
                <List/>
                <Form/>
            </Fragment>
        );
    }
}

export default App;
