const initialState = {
    messages: [],
    imageLoaded: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_MESSAGES_SUCCESS':
            return {...state, messages: action.messages};
        case 'SEND_MESSAGE_SUCCESS':
            return {...state, imageLoaded: true};
        case 'COMPLETE_LOADING_IMAGE':
            return {...state, imageLoaded: false};
        default:
            return state;
    }

};

export default reducer;