import axios from '../axios-api';

export const getMessagesSuccess = messages => ({type: 'GET_MESSAGES_SUCCESS', messages});
export const sendMessageSuccess = () => ({type: 'SEND_MESSAGE_SUCCESS'});

export const getMessages = () => {
    return dispatch => {
        return axios.get('/').then(response => {
            // console.log(response.data);
            dispatch(getMessagesSuccess(response.data))
        });
    };
};

export const sendMessage = (formData) => {
    return dispatch => {
        return axios.post('/', formData).then((response) => {
            console.log(response);
            dispatch(sendMessageSuccess())
        })
    }
};