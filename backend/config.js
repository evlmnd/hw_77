const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath: rootPath,
    uploadPath: path.join(rootPath, 'public/uploads')
};