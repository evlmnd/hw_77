const express = require('express');
const messages = require('./app/messages');
const fileDb = require('./FileDb');
const cors = require('cors');

const app = express();
fileDb.init();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());
app.use(messages);

const port = 8000;

app.listen(port, () => {
    console.log(`Server starts on ${port} port`);
});