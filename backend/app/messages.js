const express = require('express');
const fs = require('fs');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const fileDb = require('../FileDb');

const messages = express();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

messages.post('/', upload.single('image'), (req, res) => {
    console.log(req.body);
    if (req.file) {
        req.body.image = req.file.filename;
    }
    if (req.body.author === '') {
        req.body.author = 'Anonimus'
    }
    if (req.body.message === '') {
        res.send({"error": "Author and message must be present in the request"})
    } else {
        req.body.id = nanoid();
        req.body.datetime = new Date();
        fileDb.addItem(req.body);
        res.send('ok');
    }
});

messages.get('/', (req, res) => {
    const messageList = fs.readFileSync('./db.json');
    const messages = [];

    JSON.parse(messageList).slice(-30).map(message => {
            messages.push(message);
    });
    res.send(messages);
});



module.exports = messages;